#!/bin/ash

# Checks for USER variable
if [ -z "$SSHD_USER" ]; then
  echo >&2 'Please set a SSHD_USER variable (ie.: -e SSHD_USER=john).'
  exit 1
fi

# Checks for SSHD_PASSWORD variable
if [ -z "$SSHD_PASSWORD" ]; then
  echo >&2 'Please set a SSHD_PASSWORD variable (ie.: -e SSHD_PASSWORD=hackme).'
  exit 1
fi

# Set a default for SSHD_UID variable. Use the values from the official nginx image.
if [ -z "$SSHD_UID" ]; then
  SSHD_UID=101
fi

# Set a default for SSHD_GID variable. Use the values from the official nginx image.
if [ -z "$SSHD_GID" ]; then
  SSHD_GID=101
fi

# Set a default for SSHD_HOME variable.
if [ -z "$SSHD_HOME" ]; then
  SSHD_HOME=/mnt
fi

echo "Deleting user and group ${SSHD_USER}"
# Delete the user unconditionally.
deluser "${SSHD_USER}" || true
# Delete the group unconditionally.
delgroup "${SSHD_USER}" || true

echo Creating group "${SSHD_USER}"
addgroup -g "${SSHD_GID}" "${SSHD_USER}"

echo Creating user "${SSHD_USER}"
adduser -D -h "${SSHD_HOME}" -s /bin/ash -u "${SSHD_UID}" -G "${SSHD_USER}" "${SSHD_USER}" && echo "${SSHD_USER}:${SSHD_PASSWORD}" | chpasswd

echo Fixing permissions for user "${SSHD_USER}"
chown -R "${SSHD_USER}:${SSHD_USER}" "${SSHD_HOME}"

sed -i "s/Match\ User.*/Match\ User\ ${SSHD_USER}/ /etc/ssh/sshd_config"

# generate host keys if not present
ssh-keygen -A

# do not detach (-D), log to stderr (-e), passthrough other arguments
exec /usr/sbin/sshd -D -e "$@"
